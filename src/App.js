import React, { Component } from 'react';
import './App.css';
import io from 'socket.io-client';
import Popup from './components/Popup';
import Nav from './components/Nav';
import Tooltip from './components/Tooltip';
import Homepage from './pages/Homepage';
import EnterNamepage from './pages/EnterName';
import JoinHostpage from './pages/JoinHostLobby';
import Hostpage from './pages/LobbyMenu';
import Joinpage from './pages/JoinLobby';
import Gamepage from './pages/Gamepage';
import {assignmentData} from './dataFiles/gameData/Assignment';
import {battleData} from './dataFiles/gameData/Battle';
import {mostLikelyToData} from './dataFiles/gameData/MostLikelyTo';
import {triviaData} from './dataFiles/gameData/Trivia';
import {myTexts} from './dataFiles/texts';

class App extends Component {
  myGameData = {assignment:[...assignmentData], battle:[...battleData], mostLikelyTo:[...mostLikelyToData], trivia:[...triviaData]};
  roomIdleTimer;
  timerShow;

  constructor(props) {
    super(props);
    this.state = {
      connected: false,
      pageState: '',
      language: 'en',
      amountPlayers: '0',
      roomID: '',
      popup: {show: false, text: ''},
      players: [],
      roomOpen: true,
      currentGameData: {},
      everyoneSeesQuestion: false,
      personReadingQuestion: ''
    };

    this.updatePageState = this.updatePageState.bind(this);
    this.changeLanguage = this.changeLanguage.bind(this);
    this.doSocketEmit = this.doSocketEmit.bind(this);
    this.leaveSocketRoom = this.leaveSocketRoom.bind(this);
    this.closePopup = this.closePopup.bind(this);
    this.joinRoom = this.joinRoom.bind(this);
    this.kickPlayer = this.kickPlayer.bind(this);
    this.openCloseRoom = this.openCloseRoom.bind(this);
    this.hostGetNewQuestion = this.hostGetNewQuestion.bind(this);
  }

  componentDidMount() {
    if(myTexts.appName[(navigator.language || navigator.userLanguage).substring(0,2)]){
      this.setState({ language: (navigator.language || navigator.userLanguage).substring(0,2) });
    }

    this.socket = io("http://localhost:4001/");
    this.socket.io.on('connect_error', () => {
      console.log('connection error!');
      this.setState({ connected: false });
      this.updatePageState('Home');
      this.setState({ popup: {show: true, text:myTexts.popupServerOffline[this.state.language]}});
    });

    this.socket.on('connected', () => {
        this.updatePageState('Home');
        this.closePopup();
        this.setState({ roomID: '' });
        this.setState({ players: [] });
        this.setState({ roomOpen: true });
        this.setState({ currentGameData: {} });
        this.setState({ connected: true });
    });
    this.socket.on('amountPlayersOnline', data =>
        this.setState({ amountPlayers: data })
    );
    this.socket.on('roomID', data =>
      this.setState({ roomID: data })
    );
    this.socket.on('noRooms', () => {
      this.setState({ popup: {show: true, text:myTexts.popupNoRooms[this.state.language]}});
      this.updatePageState('JoinHostLobby');
    });
    this.socket.on('playerJoin', data => {
      this.setState(prevState => ({
        players: [...prevState.players, {name: data, score: 0, tempData: ''}]
      }));
    });
    this.socket.on('playerLeave', data => {
      let removeData = this.state.players.find(obj => { return obj.name === data });
      this.setState({players: this.state.players.filter(function(player) {
        return player !== removeData;
      })});
    });
    this.socket.on('openCloseRoom', data => {
      this.setState({ roomOpen: data });
    });
    this.socket.on('receivePlayerData', data => {
      this.setState(state => ({
        players: data
      }));
    });
    this.socket.on('playerWantsPlayerData', (data) => {
      this.socket.emit('givePlayerDataToPlayer', data, this.state.players);
    });
    this.socket.on('kicked', () => {
      this.updatePageState('EnterName');
      this.setState({ popup: {show: true, text:myTexts.popupKicked[this.state.language]}});
    });
    this.socket.on('roomInactive', () => {
      this.updatePageState('Home');
      this.setState({ popup: {show: true, text:myTexts.popupRoomClosedInactive[this.state.language]}});
    });
    //Now all the socket calls for the game will come
    this.socket.on('getNewGameData', (gameType, gameNumber, newGameData, personReadingQuestionIndex) => {
      this.setState({ personReadingQuestion: personReadingQuestionIndex});
      this.myGameData[gameType].splice(gameNumber, 1);
      this.updatePageState('Game');
    });

    this.socket.open();
  }

  updatePageState(param) {
    this.setState(state => ({
      pageState: param
    }));

    if(param === 'HostLobby'){
      this.setState(state => ({
        players: [{name: localStorage.getItem('playerName'), score: 0, tempData: ''}]
      }));

      this.doSocketEmit('createRoom');
      //make sure the game data is reset again.
      this.myGameData = {assignment:[...assignmentData], battle:[...battleData], mostLikelyTo:[...mostLikelyToData], trivia:[...triviaData]};
      this.updatePageState('LobbyMenu');
    }
  }

  changeLanguage(lang){
    this.setState({ language: lang });
  }

  doSocketEmit(callback, param){
    this.socket.emit(callback, param);
  }

  leaveSocketRoom(){
    this.setState({ roomID: '' });
    this.doSocketEmit('leaveRoom', this.state.roomID);
  }

  joinRoom(roomCode){
    this.setState(state => ({
      players: []
    }));

    this.socket.emit('joinRoom', roomCode, (status, name) => {
      if(status === 'success'){
        localStorage.setItem('playerName', name);
        this.setState({ roomID: roomCode });
        this.setState({ roomOpen: true });
        //make sure the game data is reset again.
        this.myGameData = {assignment:[...assignmentData], battle:[...battleData], mostLikelyTo:[...mostLikelyToData], trivia:[...triviaData]};
        this.updatePageState('LobbyMenu');
      }else{
        this.setState({ popup: {show: true, text:myTexts.popupNoRoomsFound[this.state.language]}});
      }
    });
  }

  kickPlayer(name){
    let player = this.state.players.find(obj => {
      return obj.name === name
    })
    let i = this.state.players.indexOf(player);
    this.socket.emit('kickPlayer', this.state.roomID, name, i);
  }

  closePopup(){
    this.setState({ popup: {show: false, text: ''}});
  }

  openCloseRoom(){
    this.doSocketEmit('openCloseRoom', this.state.roomID);
  }

  hostGetNewQuestion(){
    //This function can only be called by the host and is meant to choose a random new question and send it to the players.
    if(this.state.players[0].name === localStorage.getItem('playerName')){
      let games = Object.keys(this.myGameData);
      let gameType = games[(Math.floor(Math.random() * games.length))];
      let nextGameNumber = Math.floor(Math.random() * this.myGameData[gameType].length);

      if(!this.state.everyoneSeesQuestion){
        let randomPlayerNumber = Math.floor(Math.random() * this.state.players.length);
        this.socket.emit('nextQuestion', this.state.roomID, gameType, nextGameNumber, this.myGameData[gameType][nextGameNumber], randomPlayerNumber);
      }else{
        this.socket.emit('nextQuestion', this.state.roomID, gameType, nextGameNumber, this.myGameData[gameType][nextGameNumber], '');
      }
    }
  }

  render(){
    const { popup } = this.state;
    return (
      <div className="App">
        {popup.show && <Popup closePopup={this.closePopup} text={popup.text} Option2={myTexts.textClose[this.state.language]} />}
        <Tooltip text={myTexts.tooltipTiltPhone[this.state.language]} />
        <Nav pageState={this.state.pageState} changeLanguage={this.changeLanguage} language={this.state.language} updatePageState={this.updatePageState} leaveSocketRoom={this.leaveSocketRoom} />
        {
          {
            'Home': <Homepage updatePageState={this.updatePageState} language={this.state.language} connected={this.state.connected} />,
            'EnterName': <EnterNamepage updatePageState={this.updatePageState} doSocketEmit={this.doSocketEmit} language={this.state.language} />,
            'JoinHostLobby': <JoinHostpage updatePageState={this.updatePageState} language={this.state.language} />,
            'JoinLobby': <Joinpage updatePageState={this.updatePageState} joinRoom={this.joinRoom} language={this.state.language} />,
            'LobbyMenu': <Hostpage players={this.state.players} updatePageState={this.updatePageState} language={this.state.language} hostGetNewQuestion={this.hostGetNewQuestion} openCloseRoom={this.openCloseRoom} roomOpen={this.state.roomOpen} roomID={this.state.roomID} kickPlayer={this.kickPlayer} />,
            'Game': <Gamepage language={this.state.language} currentGameData={this.state.currentGameData} />,
          }[this.state.pageState]
        }
        <p id="amountPlayers">{myTexts.textCurrently[this.state.language] + ' ' + this.state.amountPlayers + ' ' + myTexts.textPlayers[this.state.language]}</p>
      </div>
    );
  }
}

export default App;
