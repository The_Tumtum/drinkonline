import React, { Component } from 'react'
import './EnterName.css';
import Button from '../components/Button'
import {myTexts} from '../dataFiles/texts'

class EnterName extends Component{
  constructor(props) {
      super(props);
      this.handleKeyUp = this.handleKeyUp.bind(this);
      this.checkInput = this.checkInput.bind(this);
      this.nameInput = React.createRef();
  }

  handleKeyUp(event) {
    if(event.keyCode === 13){
      this.checkInput();
    }
  }

  handleButtonClick(){
    this.checkInput();
  }

  checkInput(){
    let value = this.nameInput.current.value;

    if(value.length >= 2 && value.length <= 10){
      localStorage.setItem('playerName', value);
      this.props.updatePageState('JoinHostLobby');
      this.props.doSocketEmit('setNickname', localStorage.getItem('playerName'));
    }else{
      console.log("OOPS!");
    }
  }

  render() {
    return(
      <div id="contentContainer">
        <input  ref={this.nameInput} type="text" id="nameInputField" name="drinkOnName" placeholder={myTexts.placeholderName[this.props.language]} defaultValue={localStorage.getItem('playerName')} minLength="2" maxLength="10" onKeyUp={this.handleKeyUp} />
        <p id="faultyInput">{myTexts.textUsernameTooShort[this.props.language]}</p>
        <Button text={myTexts.buttonEnterName[this.props.language]} size='big' color='red' position='center' onClick={this.checkInput} />
      </div>
    );
  }
}

export default EnterName;
