import React, { Component } from 'react'
import './JoinHostLobby.css';
import Button from '../components/Button'
import {myTexts} from '../dataFiles/texts'

class JoinHostLobby extends Component{
  render() {
    return(
      <div id="contentContainer">
        <div className="bigActionButtonContainer createLobbyButtonContainer">
          <Button text={myTexts.buttonCreateRoom[this.props.language]} size='big' color='red' position='center' onClick={() => this.props.updatePageState('HostLobby')} />
        </div>
        <div className="bigActionButtonContainer joinLobbyButtonContainer">
          <Button text={myTexts.textJoinRoom[this.props.language]} size='big' color='red' position='center' onClick={() => this.props.updatePageState('JoinLobby')} />
        </div>
      </div>
    );
  }
}

export default JoinHostLobby;
