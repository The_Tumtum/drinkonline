import React, { Component } from 'react'
import './LobbyMenu.css';
import PlayerCard from '../components/PlayerCard';
import {myTexts} from '../dataFiles/texts';

class LobbyMenu extends Component{
  render() {
    return(
      <div id="contentContainer">
        <h1 className="pageHeaderLobby">{myTexts.textRoom[this.props.language]}</h1>
        <label id="roomCodeLabel" to="roomCode">{myTexts.textCode[this.props.language]}:</label>
        <p id="roomCode" name="roomCode">{this.props.roomID}</p>
        <table id="lobbyButtonAlignTable">
          <tbody>
            <tr>
              <td><button id="openCloseLobby" className={(this.props.roomOpen?'open':'closed') + (this.props.players.length !== 0 && this.props.players[0].name === localStorage.getItem('playerName')?' host':'')} onClick={() => this.props.openCloseRoom()}>{this.props.roomOpen?myTexts.textOpen[this.props.language]:myTexts.textClosed[this.props.language]}</button></td>
              <td><button id="playButton" onClick={() => this.props.hostGetNewQuestion()}>{myTexts.textPlay[this.props.language]}</button></td>
              <td></td>
            </tr>
          </tbody>
        </table>
        <div id="playerContainer">
          {this.props.players.map((player, i) => {
            return <PlayerCard key={i} number={i} playerName={player.name} language={this.props.language} host={(this.props.players[0].name === localStorage.getItem('playerName'))} kickPlayer={this.props.kickPlayer} />
          })}
        </div>
      </div>
    );
  }
}

export default LobbyMenu;
