import React, { Component } from 'react'
import './JoinLobby.css';
import Button from '../components/Button'
import {myTexts} from '../dataFiles/texts'

class JoinLobby extends Component{
  constructor(props) {
      super(props);
      this.handleKeyUp = this.handleKeyUp.bind(this);
      this.checkInput = this.checkInput.bind(this);
      this.roomcodeInput = React.createRef();
  }

  handleKeyUp(event) {
    if(event.keyCode === 13){
      this.checkInput();
    }
  }

  handleButtonClick(){
    this.checkInput();
  }

  checkInput(){
    let value = this.roomcodeInput.current.value;

    if(value.length === 6){
      this.props.joinRoom(value.toUpperCase());
    }else if(value.length > 6){
      console.log(myTexts.consoleMessageRoomCodeShort[this.props.language]);
    }
  }

  render() {
    return(
      <div id="contentContainer">
        <h1 className="pageHeader">{myTexts.textJoinRoom[this.props.language]}</h1>
        <input ref={this.roomcodeInput} type="text" id="roomcodeInputField" name="drinkOnCode" placeholder={myTexts.placeholderRoomCode[this.props.language]} minLength="6" maxLength="6" onKeyUp={this.handleKeyUp} />
        <p id="faultyInput">{myTexts.textRoomCodeTooShort[this.props.language]}</p>
        <Button text={myTexts.textJoin[this.props.language]} size='big' color='red' position='center' onClick={this.checkInput} />
      </div>
    );
  }
}

export default JoinLobby;
