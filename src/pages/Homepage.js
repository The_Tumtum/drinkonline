import React, { Component } from 'react'
import './Homepage.css';
import Button from '../components/Button'
import {myTexts} from '../dataFiles/texts'

class Homepage extends Component{
  render() {
    return(
      <div id="contentContainer">
        <img id="titleIcon" src="./image/logo.png" title={myTexts.logoAlt[this.props.language]} alt={myTexts.logoAlt[this.props.language]} />
        <h1 id="titleName">{myTexts.appName[this.props.language]}</h1>
        <Button text={myTexts.textPlay[this.props.language]} size='big' color='red' position='center' onClick={() => this.props.updatePageState('EnterName')} buttonDisabled={!this.props.connected}/>
      </div>
    );
  }
}

export default Homepage;
