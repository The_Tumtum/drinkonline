import React, { Component } from 'react'
import './Tooltip.css';
import {myTexts} from '../dataFiles/texts';

class Tooltip extends Component {
  render() {
    return(
      <div>
        <input type="checkbox" name="tooltip" id="tooltipCheck" />
        <div id="tooltip">
          <p>{this.props.text}</p>
          <label htmlFor="tooltipCheck" className="tooltipCloseIcon"><img id="tooltipCloseIcon" src="icon/close-black-18dp.svg" alt={myTexts.tooltipCloseIconAlt[this.props.language]}/></label>
        </div>
      </div>
    );
  }
}

export default Tooltip;
