import React, { Component } from 'react'
import './Button.css';

class Button extends Component {
  render() {
    return(
      <div className={this.props.size+"ActionButtonContainer "+this.props.position}>
        <button className={this.props.size+"ActionButton "+this.props.color} onClick={this.props.onClick} disabled={this.props.buttonDisabled}>{this.props.text}</button>
      </div>
    );
  }
}

export default Button;
