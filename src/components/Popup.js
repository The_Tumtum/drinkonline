import React, { Component } from 'react'
import './Popup.css';
import Button from './Button';

class Popup extends Component {
  render(){
    return(
      <div id="popupContainer">
        <div id="popup">
          <p id='popupText'>{this.props.text}</p>
            { !this.props.Option1 &&
              <div id='buttonWrap'>
                <Button text={this.props.Option2} size='small' color='red' position='center' onClick={() => this.props.closePopup()} />
              </div>
            }
            { this.props.Option1 &&
              <div id='buttonWrap'>
                <Button text={this.props.Option1} size='small' color='red' position='left' onClick={() => this.props.leaveLobby()} />
                <Button text={this.props.Option2} size='small' color='green' position='right' onClick={() => this.props.closePopup()} />
              </div>
            }
        </div>
      </div>
    );
  }
}

export default Popup;
