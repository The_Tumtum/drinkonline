import React, { Component } from 'react';
import './PlayerCard.css';
import {myTexts} from '../dataFiles/texts';

class PlayerCard extends Component {
  render() {
    return(
      <div className="cardContainer">
        <div className="card">
          {this.props.number === 0 && <img className="hostIcon pcardIcon" src="./image/logo.png" title={myTexts.textHost[this.props.language]} alt={myTexts.roomHostImageAlt[this.props.language]} />}
          <p className='playerName'>{this.props.playerName}</p>
          {this.props.number !== 0 && this.props.host && <img className="kickIcon pcardIcon" src="./icon/person_remove-white-18dp.svg" title={myTexts.textKickPlayer[this.props.language]} alt={myTexts.roomKickPlayerAlt[this.props.language]} onClick={() => this.props.kickPlayer(this.props.playerName)} />}
        </div>
      </div>
    );
  }
}

export default PlayerCard;
