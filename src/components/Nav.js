import React, { Component } from 'react'
import './Nav.css';
import Popup from './Popup';
import {myTexts} from '../dataFiles/texts';

class Nav extends Component {
  constructor(props) {
      super(props);
      this.state = {showPopup: false};
      this.clickBackButton = this.clickBackButton.bind(this);
      this.closePopup = this.closePopup.bind(this);
      this.leaveLobby = this.leaveLobby.bind(this);
  }

  clickBackButton(){
    switch(this.props.pageState) {
      case "EnterName":
        this.props.updatePageState('Home');
        break;
      case "JoinHostLobby":
        this.props.updatePageState('EnterName');
        break;
      case "JoinLobby":
        this.props.updatePageState('JoinHostLobby');
        break;
      default:
        this.props.updatePageState('Home');
        break;
    }
  }

  leaveLobby(){
    if(this.props.pageState === 'LobbyMenu' || this.props.pageState === 'Game'){
      this.props.leaveSocketRoom();
      this.props.updatePageState('EnterName');
    }
    this.setState({ showPopup: false });
  }

  closePopup(){
    this.setState({ showPopup: false });
  }

  render(){
    const { showPopup } = this.state;

    let backbutton;
    switch(this.props.pageState){
      case 'EnterName':
      case 'JoinHostLobby':
      case 'JoinLobby':
        backbutton = <img id="backButton" className="topMenuIcon" src="./icon/arrow_back-white-48dp.svg" alt={myTexts.buttonPageBackAlt[this.props.language]} title={myTexts.buttonPageBackAlt[this.props.language]} onClick={this.clickBackButton} />
        break;
      case 'LobbyMenu':
      case 'Game':
        backbutton = <img id="quitButton" className="topMenuIcon" src="./icon/exit_to_app-white-18dp.svg" alt={myTexts.buttonQuitRoomAlt[this.props.language]} title={myTexts.buttonQuitRoomAlt[this.props.language]} onClick={() => this.setState({ showPopup: true })} />
        break;
      default:
        break;
    }
    return(
      <div id="topMenuBar">
        {showPopup && <Popup updatePageState={this.updatePageState} closePopup={this.closePopup} text={myTexts.popupLeaveRoom[this.props.language]} Option1={myTexts.textLeave[this.props.language]} Option2={myTexts.textStay[this.props.language]} leaveLobby={this.leaveLobby} />}
        { backbutton }
      </div>
    );
  }
}

export default Nav;
