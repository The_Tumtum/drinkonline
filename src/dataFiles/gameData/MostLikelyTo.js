export const mostLikelyToData = [
  //statement contains the most likely to statement that will be shown
  //options is a single number with the maximum amount of random players to choose from. 0 means all players can be chosen
  //take is the amount of sip the loser or most voted player must drink.
  //give is the amount of sip the winner or most voted player can give away.
  {
    statement : "Who is most likely to die in a stupid way",
    options : 0,
    take: 5,
    give: 0
  },
  {
    statement : "Who is most likely to own 30 cats",
    options : 2,
    take: 5,
    give: 0
  },
];

export default {
    mostLikelyToData,
}
