export const triviaData = [
  //question is the trivia question that will be asked
  //options are the possible answers. The first answer needs to be the correct answer
  //take is the amount of sips the loser or most voted player must drink.
  //give is the amount of sip the winner or most voted player can give away.
  {
    question : "What is the first videogame?",
    options : ['Pong', 'Atari', 'Mario', 'Space Invaders'],
    take: 1,
    give: 1
  },
  {
    question : "What was the single most sold game ever at the end of 2020?",
    options : ['Minecraft', 'GTA: V', 'Tetris', 'Wii Sports'],
    take: 1,
    give: 1
  },
];

export default {
    triviaData,
}
