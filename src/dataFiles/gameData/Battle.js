export const battleData = [
  //the data contains the battle. {player} will be the place of a random player name
  //voters is a single number with the maximum amount of random players that judge the assignment. 0 means all players will judge
  //take is the amount of sips the loser must drink.
  //give is the amount of sip the winner can give away.
  {
    battle : "{player} and {player} do a waterfall competition. Whoever keeps going the longest is the winner",
    voters : 0,
    take: 5,
    give: 3
  },
  {
    battle : "battle of the woofs! {player} and {player} do their best wolf howl. The rest will be the judge",
    voters : 0,
    take: 0,
    give: 4
  },
  {
    battle : "{player} and {player}, who can imitate a pig the best? The others will vote",
    voters : 0,
    take: 0,
    give: 4
  },
  {
    battle : "Your price winning turkey, {player} and {player} lets hear it! Who sounds the best? Others will vote",
    voters : 0,
    take: 0,
    give: 4
  },
];

export default {
    battleData,
}
