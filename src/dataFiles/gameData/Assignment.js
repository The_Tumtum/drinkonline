export const assignmentData = [
  //the data contains the assignment {player} will be the place of a random player name
  //voters is a single number with the maximum amount of random players that judge the assignment. 0 means all players will judge
  //take is the amount of sips the loser must drink.
  //give is the amount of sips the winner can give away.
  {
    assignment : "{player}, grab a beer and a straw. Now finish your beer as quick as possible",
    voters : 2,
    take: 15,
    give: 7
  },
  {
    assignment : "What would a cow in a metalband look like? {player} will give us a demonstration",
    voters : 0,
    take: 6,
    give: 3
  },
];

export default {
    assignmentData,
}
