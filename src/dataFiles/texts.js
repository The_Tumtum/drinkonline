export const myTexts = {
  appName:{
    'en': "Drinkon!",
    'nl': "Drinkon!"
  },

  //stand alone words en scentences
  textCurrently:{
    'en': "Currently",
    'nl': "Momenteel"
  },
  textPlayers:{
    'en': "Players",
    'nl': "Spelers"
  },
  textHost:{
    'en': "Host",
    'nl': "Gastheer"
  },
  textKickPlayer:{
    'en': "Kick player",
    'nl': "Speler wegsturen"
  },
  textCode:{
    'en': "Code",
    'nl': "Code"
  },
  textJoin:{
    'en': "Join",
    'nl': "Toetreden"
  },
  textRoom:{
    'en': 'Room',
    'nl': 'Kamer'
  },
  textClose:{
    'en': "Close",
    'nl': "Sluiten"
  },
  textClosed:{
    'en': "Closed",
    'nl': "Gesloten"
  },
  textOpen:{
    'en': "Open",
    'nl': "Open"
  },
  textLeave:{
    'en': "Leave",
    'nl': "Verlaat"
  },
  textStay:{
    'en': "Stay",
    'nl': "Blijf"
  },
  textPlay:{
    'en': "Play",
    'nl': "Spelen"
  },
  textUsernameTooShort:{
    'en': "The username is too short.",
    'nl': "De gebruikersnaam is te kort."
  },
  textRoomCodeTooShort:{
    'en': "The room code is too short.",
    'nl': "De kamer code is te kort."
  },
  textJoinRoom:{
    'en': "Join room",
    'nl': "Kamer toetreden"
  },

  //tooltip texts
  tooltipTiltPhone:{
    'en': "For optimal use tilt your phone to portrait mode.",
    'nl': "Kantel je telefoon naar portret modus voor optimaal gebruik."
  },

  //Popup texts
  popupNoRooms:{
    'en': "WOW! Theres no more rooms availabe. Were very sorry, Try again later!",
    'nl': "WOW! Er zijn geen kamers meer beschikbaar. Onze excuses voor het ongemak, probeer het later nog eens!"
  },
  popupKicked:{
    'en': "You were kicked by the host.",
    'nl': "Je bent weggestuurd door de gastheer."
  },
  popupRoomClosedInactive:{
    'en': "The room was closed due to inactivity.",
    'nl': "De kemer is gesloten vanwegen inactiviteit."
  },
  popupNoRoomsFound:{
    'en': "No room could be found with this code.",
    'nl': "Er zijn geen kamers gevonden met deze code."
  },
  popupLeaveRoom:{
    'en': "Are you sure you want to leave the lobby?",
    'nl': "Weet je zeker dat je de kamer wilt verlaten?"
  },
  popupServerOffline:{
    'en': "The server seems to be offline! We will be back as soon as possible.",
    'nl': "Er kan geen connectie worden gemaakt met de server. We zijn zo snel mogelijk weer terug!"
  },

  //alt texts
  logoAlt:{
    'en': "DrinkOn! Drinkonline online drinking game logo.",
    'nl': "DrinkOn! Drinkonline online drankspel logo."
  },
  buttonPageBackAlt:{
    'en': "DrinkOn! DrinkOnline online drinking game page back icon/button.",
    'nl': "DrinkOn! DrinkOnline online drankspel pagina terug icoon/knop."
  },
  buttonQuitRoomAlt:{
    'en': "DrinkOn! DrinkOnline online drinking game quit lobby icon/button.",
    'nl': "DrinkOn! DrinkOnline online drankspel verlaat kamer icoon/knop."
  },
  roomHostImageAlt:{
    'en': "DrinkOn! Drinkonline online drinking game room host logo.",
    'nl': "DrinkOn! Drinkonline online drankspel kamer gastheer logo."
  },
  roomKickPlayerAlt:{
    'en': "DrinkOn! Drinkonline online drinking game kick player icon.",
    'nl': "DrinkOn! Drinkonline online drankspel kamer speler wegsturen icoon."
  },
  tooltipCloseIconAlt:{
    'en': "DrinkOn! DrinkOnline online drinking game tooltip close icon.",
    'nl': "DrinkOn! DrinkOnline online drankspel tooltip sluit icoon."
  },

  //placeholder texts
  placeholderName:{
    'en': "Enter name...",
    'nl': "Typ je naam..."
  },
  placeholderRoomCode:{
    'en': "Enter code...",
    'nl': "Voor code in..."
  },

  //button texts
  buttonEnterName:{
    'en': "Enter name",
    'nl': "Kies naam"
  },
  buttonCreateRoom:{
    'en': "Create room",
    'nl': "Kamer maken"
  },

  //console messages
  consoleMessageRoomCodeShort:{
    'en': "OOPS! Please dont play with the code :') We dont want you trying to break things! The room code does not have a length of 6 characters.",
    'nl': "OEPS! Speel alsjeblieft niet met de code :') We willen niet dat iemand iets kapot maakt! De kamer code heeft geen lengte van 6 karakters."
  }
};

export default {
    myTexts,
}
